#!/usr/bin/python
# -*- coding: utf-8 -*-

##	Autores
	#Espinosa Curiel Oscar
	#Gómez Flores Patricia Nallely
##


#	Script que cifra y descifra archivos VBN a exe


import optparse
from binascii import hexlify, unhexlify
import os.path
import time
import os
from random import randint
import shutil
from re import search

def add_options():
	parser = optparse.OptionParser()
	parser.add_option('-c','--cipher', dest='cipher',help='Cifrar el archivo')
	parser.add_option('-d', '--decrypt', dest='decrypt', help='Descifrar un archivo')
	parser.add_option('-k','--key', dest='key',default=None,help='Llave para cifrado')
	parser.add_option('-m', '--magicNumber', dest='magicNumber', default="4D5A900003", 
		help='Numero magico a buscar para descifrar el archivo')

	opts,args = parser.parse_args()

	return opts

def readBinFile(bin_file):
    '''
		Funcion que lee el contenido binario del archivo
		dado como argumento
    '''
    return open(bin_file,'rb').read()

def do_xor(bin_file,key=0xA5):
    '''
		Funcion que realiza XOR a cada byte del string dado
		Devuelve el nuevo string cifrado con XOR con la llave
    '''
    ciphertext = ''
    for i in range(len(bin_file)):
        ciphertext += chr(ord(bin_file[i]) ^ key)

    return ciphertext

def do_decrypt(decrypt_file, decrypt_key, magicNumber):
	'''
		Funcion que realiza el descifrado del archivo VBN

		Devuelve, en caso de encontrar el magicNumber contenido, el string
		a partir de este magicNumber. Si no encuentra el magicNumber, devuelve
		None.
	'''

	#Leemos el contenido y hacemos XOR
	content = readBinFile(decrypt_file)
	xor_content = do_xor(content,decrypt_key)

	#REGEX que se busca para encontrar el magicNumber
	reg = ".*(" + magicNumber + ".*)"
	re1 = search(reg, hexlify(xor_content).upper())
	if re1:
		#Si hubo coincidencia, devuelve el bytearray binario
		return unhexlify(re1.group(1))
	else:
		#Si no hubo coincidencia, devuelve None
		return None

def BF_decrypt(decrypt_file, magicNumber= "4D5A900003"):
	'''
		Funcion que realiza la fuerza bruta para encontrar la llave de un byte
		con que esta cifrada el archivo, se basa en que do_decrypt devuelve un
		string del archivo cuando encuentra el magicNumber, si no lo devuelve, esa
		no es la llave con que se ha cifrado
	'''
	for key in range(256):
		original_file = do_decrypt(decrypt_file, key, magicNumber)
		#Si devuelve el archivo, encontro el magicNumber, entonces esta es la llave con que
		#se ha cifrado
		if original_file:
			print "\tLlave encontrada:\t" + hex(key)
			return (original_file, key)
	
	return None
	
def getOriginalFileName(bin_file):
	'''
	Funcion que obtiene el nombre del archivo original de la cabecera hecha por
	Symantec
	'''
	temp = open(bin_file, "rb").read()[4:100]
	temp = temp[temp.rfind("\\")+1:]
	# Quitamos los caracteres inecesarios del nombre del archivo
	fileName = temp.rstrip(' \t\r\n\0')

	return fileName

def writeOriginalFile(original_name, original_file):
	'''
		Funcion que escribe un bytearray binario a un archivo
		con el nombre del primer argumento.
	'''
	with open(original_name, "wb") as final:
		final.write(original_file);
	print "Se ha creado el archivo " + original_name

def do_cipher(cipher_file, cipher_key):
	cifrado = ''
	malformada = ''

	if cipher_file.find('exe') != -1: #--Verificar extensión del archivo
		extension = 'exe'
	else:
		print 'El archivo no es un archivo .exe!'
		exit(1)

	actual = os.getcwd()	#--Creacion de directorio para guardar el archivo
	directorio = os.path.join(actual,"Cuarentena")

	if not os.path.exists(directorio): #--Verifica la existencia del archivo
		nuevo = os.mkdir('Cuarentena',0o744) #-- Si no existe lo crea

	print 'Creacion del archivo...\n'

	archivo = open(cipher_file,"rb").read() #--Apertura del archivo

	for i in range (len(archivo)): #--Operacion xor entre el archivo y la llave
		cifrado += chr(cipher_key ^ ord(archivo[i])) #- Por el problema de la conversión y los
												# - distintos caracteres que el archivo tiene,
												# - se hace uso de ord, para obtener su valor en ascii
												# - y se chr para obtener el equivalente a este en caracteres

	for i in range(50): #-- Creacion de una cadena para colocar al inicio del archivo
		malformada +=chr(randint(0,255))

	carpeta = os.path.join(actual,cipher_file) #-- Carpeta donde se encuentra el archivo a cifrar
	nombre = "57"+chr(randint(65,90))+time.strftime("%d%m%y")+chr(randint(97,122))+".VBN"
	salida = open(nombre,'wb') #--EScritura en el archivo
	salida.write(malformada)
	salida.write(carpeta)
	salida.write(".........."+cipher_file+"...................")
	salida.write(cifrado)
	salida.close()
	shutil.move(nombre,directorio) #--Para mover el archivo creado a la carpeta cuarentena
	print 'El archivo a sido cifrado, se encuentra en',directorio,'\n'
	print 'Con el nombre',nombre,'\n'

def getQuarantineFile_main(options):
	'''
		Funcion principal del script. Recibe el OptionParser donde saca el archivo y la llave.
	'''

	#El archivo es importante, si no se da archivo, se termina el programa
	if options.cipher is None and options.decrypt is None:
		print 'Debes indicar un archivo a cifrar o descifrar'
		exit(1)

	#Se eligio cifrado del archivo
	elif options.cipher is not None:
		if not os.path.exists(options.cipher):
			print 'El archivo indicado no existe!'
			exit(1)

		#Se dio una llave para descifrar
		if options.key is not None:
			llave = int(options.key,16)

			#La llave debe ser de un byte
			if llave > 255:
				print 'La llave debe ser de un byte'
				exit(1)
			else:
				do_cipher(options.cipher, llave)
				exit(0)
		
		#No se dio llave para descifrar, se asigna por defecto la llave 0xa5
		else:
			llave = 0xA5
			do_cipher(options.cipher, llave)
			#Termino exitoso
			exit(0)

	#Se eligio descifrar el archivo
	elif options.decrypt is not None:
		if not os.path.exists(options.decrypt):
			print 'El archivo indicado no existe!'
			exit(1)
		
		#magicNumber = "4D5A900003"
		if options.key is not None:
			llave = int(options.key,16)
			if llave > 255:
				print 'La llave debe ser de un byte'
				exit(1)
			else:
				original_file = do_decrypt(options.decrypt, llave, options.magicNumber)
				if original_file:
					original_name = getOriginalFileName(options.decrypt)
					writeOriginalFile(original_name, original_file)

				exit(0)
		else:
			print "BF"
			#decifrado por bruteforce
			original_file, key = BF_decrypt(options.decrypt, options.magicNumber)
			if original_file:
				original_name = getOriginalFileName(options.decrypt)
				writeOriginalFile(original_name, original_file)

if __name__ == '__main__':
	
	opts = add_options()
	getQuarantineFile_main(opts)
	