#!/bin/python

#-----------------
#   Oscar Espinosa
#-----------------

#
#   Script que busca archivos dentro de un archivo
#   (captura de red, particion, etc) binario.


import optparse
import sys
from binascii import hexlify
from sys import exit

#global var
magic_numbers = {}
matches = {}
count_files = 0
#EOFile = ['ae', '42', '60', '82']

def add_options():
    """
        Funcion para obtener los argumentos del script
    """
    parser = optparse.OptionParser()
    parser.add_option('-f', '--file', dest='file',help='Input binary file to scan')
    parser.add_option('-c', '--config_file', dest='conf', default='File_carving.conf', help='Configuration file of this tools')
    #Medidos en KB
    parser.add_option('-l', '--length', dest='length', default=5000, type=int, help='Max size to search when a file is found')
    opts, args = parser.parse_args()

    return opts

def printError(msg, leave = 0):
    print msg
    if leave == 1:
        sys.exit(1)

def get_configuration(confN):
    global magic_numbers, little
    try:
        with open(confN, 'r') as confFile:
            for line in confFile:
                if line[0] != "#" and line != "\n":
                    key, value = line.strip("\n").split("=")
                    magic_numbers[key] = value.split()
                    matches[key] = 0

    except IOError:
        printError('El archivo ' + confN + ' no existe o no se tienen permisos de lectura', 1)

def get_file_length(file_type, binFile):
    f_length = 0
    current_byte = binFile.tell()
    if file_type == 'png':
        binFile.seek(4,1)
        f_length = int(hexlify(binFile.read(4)), 16)
        #CRC de png
        #http://www.libpng.org/pub/png/spec/1.2/PNG-Chunks.html seccion 4.1.1
        f_length += 8
    elif file_type == 'zip':
        binFile.seek(14,1)
        f_length = int(hexlify(binFile.read(4)), 16)
        #https://pkware.cachefly.net/webdocs/casestudies/APPNOTE.TXT seccion 4.3.7
        f_length += 26
    elif file_type == 'gif':
        f_length = int(hexlify(binFile.read(1)), 16)
        #https://www.w3.org/Graphics/GIF/spec-gif89a.txt seccion 15.c
        f_length += 1
    elif file_type == 'jpg':
        binFile.seek(2,1)
        f_length = int(hexlify(binFile.read(2)), 16)
        #http://www.vip.sugovica.hu/Sardi/kepnezo/JPEG%20File%20Layout%20and%20Format.htm seccion Format of each segment
        f_length += 2
    
    binFile.seek(current_byte)
    return f_length


def search_file(binFile, file_type, max_length):
    global count_files, magic_numbers
    n = 0
    data = ''
    file_length = get_file_length(file_type, binFile)
    if file_length < max_length:
        #Leemos el archivo y almacenamos
        while n < file_length:
            data += binFile.read(1)
            n += 1

        with open("file" + str(count_files) + "." + file_type, 'wb') as wfile:
            if file_type == 'png':
                wfile.write("\x89\x50\x4e\x47\x0d\x0a\x1a\x0a")
            elif file_type == 'zip':
                wfile.write('\x50\x4b\x03\x04')
            elif file_type == 'gif':
                wfile.write('\x47\x49\x46\x38\x37\x61')
            elif file_type == 'jpg':
                wfile.write('\xff\xd8')

            wfile.write(data)
        count_files += 1
        return True
    else:
        return False
    #print "encontrado un archivo " + file_type

def carve_file(fileN, max_length):
    global magic_numbers
    found_files  = {}
    for key2 in magic_numbers.keys():
        found_files[key2] = 0
    try:
        with open(fileN, 'rb') as binFile:
            data = binFile.read(1)
            while data:
                for key in magic_numbers.keys():
                    if magic_numbers['little-endian'][0] == '1':
                        if hexlify(data) == magic_numbers[key][:: -1][matches[key]]:
                            matches[key] +=1
                            if matches[key] == len(magic_numbers[key]):
                                for key2 in matches.keys():
                                    matches[key2] = 0
                                
                                if search_file(binFile, key, max_length) == True:
                                    found_files[key] += 1
                                    #exit(1)
                
                data = binFile.read(1)
            print found_files
    except IOError:
        printError('El archivo ' + fileN + ' no existe o no se tienen permisos de lectura', 1)

def carving_main():
    opts = add_options()

    get_configuration(opts.conf)
    carve_file(opts.file, opts.length)

if __name__ == '__main__':
    carving_main()