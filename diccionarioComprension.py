#!/usr/bin/python
#UNAM-CERT
#Oscar Espinosa Curiel

#Ejercicio de clase 6

def dicGen():
	return {n: (bin(n), hex(n)) for n in range (50) if bin(n).count('1')%2 != 0}

if __name__ == '__main__':
	print dicGen()
