#!/bin/etc/python
# -*- coding: utf-8 -*-

from sys import exit
import optparse
from binascii import hexlify
SECTOR_SIZE=512
PART_TYPES = "\n0) Empty \n7) HPFS/NTFS/ExFAT \n83) Linux \n82) Linux swap/Solaris \n86) NTFS volume Set\n"
types= {0:'Empty', 7:"HPFS/NTFS/ExFAT", 130:"Linux Swap/Solaris", 131:"Linux", 134:"NTFS volume Set" }
prim=0

def opciones():
    parser = optparse.OptionParser()
    #Recibe un argumento posicional
    parser.add_option('-l','--listar', dest='listar', action='store_true', default=None, help='Muestra las particiones de un dispositivo')
    opts,args = parser.parse_args()
    return opts, args

def menu():
        print "n) Crear nueva particion"
        print "p) Imprimir tabla de particones"
        print "w) Guardar"
        print "q) Salir"
        res= raw_input("\nElige la opcion: ")
        return res

def read_parts(mbr):
        j = 0
        parts = []
        for i in range(4):
                #Si se tienen datos en la tabla de particiones
                if int("".join(mbr[j+8:j+12][::-1]).encode('hex'), 16) !=0:
                        new_part= []
                        #Se almacenan los hex de cada particion
                        new_part.append(int("".join(mbr[j+8:j+12][::-1]).encode('hex'), 16))
                        new_part.append(int("".join(mbr[j+12:j+16][::-1]).encode('hex'), 16))
                        new_part.append(int(hex(ord(mbr[j+4]))[2:], 16))
                        parts.append(new_part)
                        j = j+16

        return parts

def display_MBR(parts):
        print ""
        cont = 1
        if len(parts)==0:
                print "No hay particiones"
        else:
                print " Device       Start      End     Sectors     Size      Type"
                for part in parts:
                        print " /dev/sdb"+str(cont)+"    "+str(part[0])+"      "+str(part[0]+part[1]-1)+"     "+str(part[1])+"   "+str((part[1]*SECTOR_SIZE)/(1024**2))+"MB      "+types[part[2]]
                        cont = cont+1
        print "\n"

def write_MBR(parts, args):
        with open(args[0], "r+b") as dev:
                dev.seek(446, 0)
                for part in parts:
                        dev.seek(2, 1)
                        dev.write(chr(0))
                        dev.write(chr(0))
                        dev.write(chr(part[2]))
                        dev.seek(3, 1)
                        content = [('{:08x}'.format(part[1])[i:i+2]) for i in range(0, 8, 2)][::-1]
                        print content
                        for byte in content:
                                dev.write(chr(int(byte, 16)))
                        content = [('{:08x}'.format(part[1])[i:i+2]) for i in range(0, 8, 2)][::-1]
                        print content
                        for byte in content:
                                dev.write(chr(int(byte, 16)))
                dev.seek(510, 0)
                dev.write('\x55\xaa')


def make_new_partition(parts, disk_size):
        global prim
        free_space = 0
        for part in parts:
                free_space = free_space + part[1]
        sec_default = 2048
        size_default = 0
        for part in parts:
                sec_default = part[1]+sec_default
                size_default = part[1] + size_default

        size_default = disk_size - size_default
        defsize=(size_default*SECTOR_SIZE)/(1024**2)
        n_part = []
        print "\nParticion " + str(len(parts) + 1)
        sec_ini=raw_input("Sector Inicial ("+str(sec_default)+"-"+str(disk_size)+", por defecto "+str(sec_default)+"):")
        size=raw_input("Tamaño en MB, por defecto "+str(defsize)+"): ")
        size_default = size if len(size)>0 else defsize
        sec_default = sec_ini if len(sec_ini)>0 else sec_default
        size_default = (int(size_default)*1024*1024)/SECTOR_SIZE
        ptype=raw_input("Elige el tipo de particion (L para ver la lista de particiones): ")
        try:
                if ptype == 'L':
                        print "\n"
                        print PART_TYPES
                        print "\n"
                        ptype=raw_input("Elige el tipo de particion: ")
                
                if int(ptype, 16) in types.keys():
                        n_part.append(int(sec_default)) #sector de inicio
                        n_part.append(int(size_default)) #cilindro (tamanio)
                        n_part.append(int(ptype, 16))
                        parts.append(n_part)
                        print "\nSe ha creado la particion de tipo " + types[int(ptype, 16)]
                        print "\n\n"
                        prim+=1
                else:
                        print" El tipo de particion no es valido\n"
        except ValueError:
                print "Error al elegir el tipo de particion\n\n"

def part_disk_menu(parts, disk_size, args):
        while(1):
                option=menu()
                if option=="p": #Imprimir tabla MBR
                        display_MBR(parts)
                elif option=="w": #Guardar cambios
                        write_MBR(parts, args)
                elif option=="q": #Salir
                        exit(0)
                elif option=="n": #Agregar una nueva particion
                        if prim==4:
                                print "No hay particiones primarias disponibles.\n"
                        else:
                                make_new_partition(parts, disk_size)

def part_disk_main():
        prim=0
        opts, args = opciones()
        if len(args) == 0:
                print "Debes dar un disco como argumento"
                print "part_disk <device> [opciones]"
                exit(1)
        try:
                with open(args[0], "rb") as disk:
                        disk.seek(0, 2)
                        disk_size = disk.tell()/SECTOR_SIZE
                        info="Tamano del sector: 512 bytes\nTotal de bloques: "+str(disk_size)
                        disk.seek(446, 0)
                        #se leen los 512 bytes de la tabla de particiones
                        mbr = list(disk.read(64))
        except IOError:
                print "Este disco no existe o no se tienen permisos de lectura"
                exit(1)

        parts=read_parts(mbr)
        if opts.listar:
                print info
                display_MBR(parts)
                exit(0)

        part_disk_menu(parts, disk_size, args)
if __name__ == '__main__':
        part_disk_main()
