#!/usr/bin/python
#UNAM-CERT
#Oscar Espinosa

#Ejercicio de clase 8

from re import match

#Expresion regular de direccion IPv4
ipv4 = r"[0-2]?[0-9]?[0-9]\.[0-2]?[0-9]?[0-9]\.[0-2]?[0-9]?[0-9]\.[0-2]?[0-9]?[0-9]"
print match(ipv4, "192.168.68.3")

#Expresion regular de correos electronicos
correo = r"[a-zA-Z0-9_\.-]+@[a-z]+\.[a-z]+\.?[a-z]*\.?[a-z]*"
print match(correo, "oscare_@outlook.com")
