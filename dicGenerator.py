#!/usr/bin/python
#UNAM-CERT
#Oscar Espinosa Curiel

"""
	Script que genera un diccionario de contrasenas a partir de cadenas dadas
	desde un archivo de entrada dado como primer argumento del script. El resultado
	se almacena en un archivo de salida dado como segundo parametro del script.
	python dicGenerator.py input.file output.file
"""
from sys import argv
from random import choice
from sys import setrecursionlimit

"""
	Tuplas usadas por las funciones para concatenar caracteres especiales y hacer
	los cambios de numeros-letras y viceversa
"""
ce = ('~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '-', '+', '=', '[', ']', '{', '}', '|', '/', '.', ',')
num = ('4','3','1','0','9','5','7')
charac = ('a','e','i','o','q','s','t')

def readFile(archivo):
	"""
		Funcion que lee el archivo donde se encuentra informacion del usuario.
		Regresa una list con las lineas de archivo
	"""
	with open(archivo, 'r') as arch:
		return arch.read().split('\n')[:-1]

def letter2num (letter):
	"""
		Funcion que cambia una letra por su homologo en numero
		retorna un caracter del numero correspondiente acorde a su posicion en 
		num y charac.
	"""
	if letter.lower() in charac:
		return num[charac.index(letter.lower())]
	else:
		return letter

def stringLetter2num (word):
	"""
		Funcion que cambia todas las coincidencias de letras definidas en charac de una cadena
		por	su homologo en num. Este cambio se hace de manera aleatoria. Si se obtiene True
		entonces se realiza el cambio (si es que el caracter se encuentra en charac),
		si se obtiene False no se realiza el cambio y se deja el caracter tal cual.
		Retorna un string con los cambios realizados.
	"""
	aux = ''
	for c in word:
		if choice((True, False)):
			aux += letter2num(c)
		else:
			aux += c
	return aux

def num2letter (number):
	"""
		Funcion que cambia un numero por su homologo en charac.
		Si este numero se encuentra en num, se hace el cambio por el correspondiente
		en charac, en caso contrario se devuelve el caracter tal cual
	"""
	if number in num:
		return charac[num.index(number)]
	else:
		return number

def stringNum2letter (word):
	"""
		Funcion que recorre una cadena y a cambia aleatoriamente los numeros de
		la cadena por su homologo en charac.
		El cambio se realiza de manera aleatoria, si se obtiene True, se hace el
		cambio (en caso que este caracter se encuentre en num), en caso contrario
		se escribe el caracter tal cual.
	"""
	aux = ''
	for c in word:
		if choice((True, False)):
			aux += num2letter(c)
		else:
			aux += c
	return aux

def swapCase (letter):
	"""
		Funcion que cambia un caracter de mayusculas a minusculas y viceversa.
		Se retorna el caracter tal cual si este no es alfabetico.
	"""
	return letter.swapcase()

def stringSwapCase (word):
	"""
		Cambia aleatoriamente los caracteres de Mayus a minus y viceversa.
		Si se obtiene True, se cambia el caracter (en caso que sea alfabetico)
		En caso contrario se escribe el caracter tal cual.
		Retorna un string con los cambios realizados.
	"""
	aux = ''
	for c in word:
		if choice((True, False)):
			aux += swapCase(c)
		else:
			aux += c
	return aux

def validateCombination(word):
    """
		Funcion que valida que una cadena dada cumpla con ciertas reglas:
			- Al menos 8 caracteres de longitud.
			- Al menos un numero.
			- Al menos una mayuscula.
			- Al menos una minuscula.
			- Al menos un caracter especial (definido en ce).
		Retorna True si todas las reglas se cumplen, False en caso contrario.
    """
    a,A,num,ch, l = False, False, False, False, False
    if len(word) >= 8:
        l = True
    for c in word:
        if a and A and num and ch:
            return True

        if c.isdigit():
            num = True
            continue
        elif c.isupper():
            A = True
            continue
        elif c.islower():
            a = True
            continue
        elif c in ce:
            ch = True

	return False

def generateCombination (word, countR = 0):
    """
		Funcion que genera combinaciones que cumplan con las reglas comunes
		de una contrasena a partir de un string dado.
		Para evitar un overloop en la pila de llamadas por la recursividad, se
		devuelve la cadena cuando se ha realizado diez veces la recursividad para
		una cadena. La condicion de paro es que la cadena cumpla con las reglas
		basicas de contrasena.
		Recorre el string y aleatoriamente se decide si se realiza un cambio al
		caracter en cuestion.
		Retorna un string modificado y validado.
    """
    #Condicion de paro de la recursividad
    if validateCombination(word) or countR == 10:
        return word

    vPasswd = ''
    for c in word:
        if c.isalpha():
            #Aleatoriamente se elige entre cambiar por numero o de tipo
            selector = choice((True, False))
            #Cambiamos letra por numero
            if selector:
                vPasswd += letter2num(c)
            #Cambiamos May-min o min-May
            else:
                vPasswd += swapCase(c)
        elif c.isdigit():
            #Aleatoriamente se elige si se cambia o no por una letra
            selector = choice ((True, False))
            if selector:
                vPasswd += num2letter(c)
            else:
                vPasswd += c
		#Cuando es un caracter especial se copia tal cual
        else:
        	vPasswd += c

    return generateCombination(vPasswd, countR + 1)

def combine(listaInfo, outText):
	"""
		Funcion que escribe en el archivo de salida distintas combinaciones
		de contrasenas a partir de las palabras leidas del archivo de entrada.
		Recorre estas palabras y las combina en pares concatenandolas con un
		caracter especial en medio, al principio y al final de la concatenacion.
		Por cada combinacion realizada, se escribe en el archivo una cadena de
		contrasena valida, una que cambia letras por numeros, una que cambia
		numeros por letras y otra que cambia de Mayus-minus y viceversa.
		Se combinan todas las palabras y por cada combinacion se generan otras
		30 combinaciones. Entonces, se generan 
	"""
	with open(outText, 'w') as out:
		#Si se tiene un solo dato, se agrega un elemento vacio para poder hacer
		#combinaciones con los caracteres especiales
		if len(listaInfo) == 1:
			listaInfo.append('')
		for i in range(len(listaInfo)):
			for j in range(i + 1,len(listaInfo)):
				for c in ce + num + ('2', '6', '8'):
					aux = listaInfo[i] + c + listaInfo[j]
					if (len(aux) >= 8 or len(aux) <= 15):
						out.write(aux + '\n')
						out.write(generateCombination(aux) + '\n')
						out.write(stringLetter2num(aux) + '\n')
						out.write(stringNum2letter(aux) + '\n')
						out.write(stringSwapCase(aux) + '\n')

					auxInvert = listaInfo[j] + c + listaInfo[i]
					if (len(auxInvert) >= 8 or len(auxInvert) <= 15):
						out.write(auxInvert + '\n')
						out.write(generateCombination(auxInvert) + '\n')
						out.write(stringLetter2num(auxInvert) + '\n')
						out.write(stringNum2letter(auxInvert) + '\n')
						out.write(stringSwapCase(auxInvert) + '\n')

					aux = c + listaInfo[i] + listaInfo[j]
					if (len(aux) >= 8 or len(aux) <= 15):
						out.write(aux + '\n')
						out.write(generateCombination(aux) + '\n')
						out.write(stringLetter2num(aux) + '\n')
						out.write(stringNum2letter(aux) + '\n')
						out.write(stringSwapCase(aux) + '\n')

					auxInvert = c + listaInfo[j] + listaInfo[i]
					if (len(auxInvert) >= 8 or len(auxInvert) <= 15):
						out.write(auxInvert + '\n')
						out.write(generateCombination(auxInvert) + '\n')
						out.write(stringLetter2num(auxInvert) + '\n')
						out.write(stringNum2letter(auxInvert) + '\n')
						out.write(stringSwapCase(auxInvert) + '\n')


					aux = listaInfo[i] + listaInfo[j] + c
					if (len(aux) >= 8 or len(aux) <= 15):
						out.write(aux + '\n')
						out.write(generateCombination(aux) + '\n')
						out.write(stringLetter2num(aux) + '\n')
						out.write(stringNum2letter(aux) + '\n')
						out.write(stringSwapCase(aux) + '\n')

					auxInvert = listaInfo[j] + listaInfo[i] + c
					if (len(auxInvert) >= 8 or len(auxInvert) <= 15):
						out.write(auxInvert + '\n')
						out.write(generateCombination(auxInvert) + '\n')
						out.write(stringLetter2num(auxInvert) + '\n')
 						out.write(stringNum2letter(auxInvert) + '\n')
						out.write(stringSwapCase(auxInvert) + '\n')


if __name__ == '__main__':
	lista = readFile(argv[1])
	combine(lista, argv[2])
