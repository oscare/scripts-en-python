#!/usr/bin/python
# -*- coding: utf-8 -*-
#UNAM-CERT
from random import choice

calificacion_alumno = {}
calificaciones = (0,1,2,3,4,5,6,7,8,9,10)
becarios = ['Juan Manual',
            'Ignacio',
            'Valeria',
            'Luis Antonio',
            'Pedro Alejandro',
            'Diana Guadalupe',
            'Jorge Luis',
            'Jesika',
            'Jesús Enrique',
            'Rafael Alejandro',
            'Servando Miguel',
            'Ricardo Omar',
            'Laura Patricia',
            'Isaías Abraham',
            'Oscar']

def asigna_calificaciones():
    for b in becarios:
        calificacion_alumno[b] = choice(calificaciones)

def reprobadosNoreprobados():
    reprobados = []
    noReprobados = []
    for alumno in calificacion_alumno:
	if calificacion_alumno[alumno] < 8:
            reprobados.append(alumno)
        else:
	    noReprobados.append(alumno)
    return tuple(reprobados), tuple(noReprobados)

def promedio():
    suma = 0.0
    for alumno in calificacion_alumno:
	suma += calificacion_alumno[alumno]

    return suma/len(calificacion_alumno)

def conjuntoCalificaciones():
    li = []
    for alumno in calificacion_alumno:
	li.append(calificacion_alumno[alumno])
    return set(li)

def imprime_calificaciones():
    for alumno in calificacion_alumno:
        print '%s tiene %s\n' % (alumno,calificacion_alumno[alumno])

asigna_calificaciones()
#imprime_calificaciones()
print reprobadosNoreprobados()
print promedio()
print conjuntoCalificaciones()
