#!/usr/bin/python3
# -*- coding: utf-8 -*-
#UNAM-CERT
#Autor: Oscar Espinosa Curiel

def maxPalindromo(word):
	"""
		maxPalindromo (string)
		return string
		Funcion que retorna el palindromo más largo dentro de una
		cadena.
		Se recorre toda la cadena y se toma cada caracter como centro del
		palindromo, otra funcion es la que se encarga de encontrar el palindromo
		mas grande con ese caracter como centro. Finalmente devuelve el palindromo
		mas largo encontrado 
	"""
	maxPal = ""
	for posicion in range(1, len(word) - 1):
		#Para palindromos con centro de una letra, como 'aba'
		aux = palindromo (posicion, word, posicion)
		#Para palindromos con centro de dos letras, como 'abba'
		aux2 = palindromo (posicion, word, posicion + 1)

		#Se valida si alguno de los dos palindromos es mas largo que el más largo
		#encontrado hasta ese momento
		if len(aux) > len(maxPal):
			maxPal = aux
		elif len(aux2) > len(maxPal):
			maxPal = aux2

	return maxPal

def palindromo (left, word, right):
	"""
		palindromo (int, string, int)
		return string

		Funcion que busca el palindromo más largo dentro de una cadena empezando en los
		indices pasados como parametros
	"""
	while left >= 0 and right < len(word):
		if word[left] == word[right]:
			left -= 1
			right += 1
		else:
			break
	#Se retorna un left+1 por el ultimo descuento que se hace dentro del condicional. No se
	#retorna right-1 porque la posicion final no es inclusiva.
	return word[left + 1:right]

print (maxPalindromo('abaabacabanitalavalatinaolo'))
print (maxPalindromo('ababba'))