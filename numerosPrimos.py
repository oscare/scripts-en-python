#!/usr/bin/python3
# -*- coding: utf-8 -*-
#UNAM-CERT
#Autor: Oscar Espinosa Curiel

def primeList(lista, count, n):
	"""
		primeList(list, int, int)
		return list
		Funcion que calcula los primeros n numero primos. Recibe como parametros
		la lista de los numeros primos, el numero en el que va contando y
		el numero de primos que se desean.
		Devuelve una lista con los primeros n primos.
		CONSIDERACION:
		si count es 1 se hace una recursividad infinita porque todos los numeros
		son divisibles por 1
	"""
	if len(lista) == n:
		return lista

	isPrime = True
	for divisor in lista:
		if count % divisor == 0:
			isPrime = False

	if isPrime:
		lista.append(count)

	return primeList(lista, count + 1, n)

def primeNumbers(n):
	"""
		primeNumbers(int)
		return list
		Funcion que recibe cuantos numeros primos se desea generar
		Llama a otra funcion que genera la lista con los numeros primos
		Esta funcion es auxiliar para validar que el numero dado sea valido
		y por el segundo argumento de primeList.
	"""
	if n >= 1:
		return primeList([], 2, n)
	else:
		return []

def isNumberPrime(lenPrimeList, n):
	"""
		isNumberPrime(int, int)
		return boolean
		Funcion que valida si un numero m es primo o no. Recibe un entero que es
		la longitud de la lista de numeros primos y el numero que se desea validar.
		Itera sobre una lista de numeros primos, si n es divisible sobre alguno
		de ellos, entonces no es primo y retorna False. Si itera toda la lista
		entonces n es primo y retorna True.
		CONSIDERACION:
		La longitud de primeList define la precision del algoritmo
		calcula primos hasta max(primeList)**2
	"""
	primeList = primeNumbers(lenPrimeList)
	for prime in primeList:
		if n % prime == 0:
			return False
	return True


print(primeNumbers(25))
print (isNumberPrime(20, 16129))