#!/usr/bin/python3
# -*- coding: utf-8 -*-
#UNAM-CERT
#Autor: Oscar Espinosa Curiel


import random
import string
import re

def secPasswdGen(passwd, patron, n):
	"""
		secPassGen(string, re, int)
		return string
		Funcion que genera contrasenas seguras acorde al patron dado.
		En caso que no se haya aceptado la contrasena para cuando es de
		longitud 15, se acepta como segura para evitar un overloop en
		la pila de llamadas

	"""
	if patron.match(passwd) != None:
		return passwd

	if n == 15:
		return passwd
	passwd += random.choice(string.printable)
	return secPasswdGen(passwd, patron, n + 1)


def genSecPasswd():

	"""
		return string
		Funcion que retorna una contrasena segura acorde a una expresion regular
		dada como patron.

		patron de contrasena segura. 
		Valida
		Minimo 8 caracteres
		Maximo 15
		Al menos una letra mayúscula
		Al menos una letra minucula
		Al menos un dígito
		Al menos 1 caracter especial

		Tomado de: 
		https://es.stackoverflow.com/questions/4300/expresiones-regulares-para-contrase%C3%B1a-en-base-a-una-politica

		CONSIDERACiON:
			acepta contrasenas con salto de linea, espacios y tabulaciones, aunque las incidencias son pocas.
	"""
	patron = re.compile('^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ][^\n]){8,15}$')
	
	return secPasswdGen('', patron, 1)

print (genSecPasswd())