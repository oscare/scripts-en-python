#!/usr/bin/python
# -*- coding: utf-8 -*-
#UNAM-CERT
#XML - parser
import sys
import xml.etree.ElementTree as ET
from datetime import datetime
from hashlib import md5

class Host:
	def __init__(self, status, ip, hostname, honeypot, ssh):
		self.status = status
		self.ip = ip
		self.hostname = hostname
		self.honeypot = honeypot
		self.ssh = ssh

	def __str__(self):
		return '%s,%s,%s,%s,%s\n' % (self.status, self.ip,
									self.hostname, self.honeypot,
									self.ssh)

def printError(msg, exit = False):
    sys.stderr.write('Error:\t%s\n' % msg)
    if exit:
        sys.exit(1)

def leeXML(archivo_xml):
	"""
		Funcion que cuenta los servidores encendidos/apagados, asi como los
		puertos por los que escucha cada servidor.
		Cada servidor se agrega a una lista de servidores que son de la clase
		Hosts.
		Retorna una tupla con la lista de servidores como primer elemento y
		las distintas cuentas que se hicieron de los servidores.
	"""
	servidores = []
	sUP = 0
	sDOWN = 0
	hostname, status, ip, portN, state = "","","","",""
	honeypot, ssh = False, False
	apache, nginx, dionaea, otros, countSSH, countDNS, countHTTP, countHTTPS, countHN = 0,0,0,0,0,0,0,0,0
	with open(archivo_xml,'r') as xmlfile:
		root = ET.fromstring(xmlfile.read())
		for host in root.findall('host'):
			status = host.find('status').get('state')
			ip = host.find('address').get('addr')
			if status == 'up':
				sUP += 1
			else:
				sDOWN += 1
			for elem in host.iter():
				try:
					if elem.tag == 'port':
						portN = elem.attrib['portid']
					if elem.tag == 'state':
						state = elem.attrib['state']

						if portN == '22' and state == 'open':
							ssh = True
							countSSH += 1
						elif portN == '53' and state == 'open':
							countDNS += 1
						elif portN == '80' and state == 'open':
							countHTTP += 1
			 				if elem.attrib['product'] == 'Dionaea Honeypot httpd':
								honeypot = True
						elif portN == '443' and state == 'open':
							countHTTPS +=1

					if elem.tag == 'service':
						if portN == '80' and state == 'open':
							try:
								server = elem.attrib['product']
								#Consideramos Apache httpd y Apache Tomcat
								if 'Apache' in server:
									apache += 1
								elif server == 'nginx':
									nginx += 1
								elif server == 'Dionaea Honeypot httpd':
									dionaea +=1
								else:
									otros += 1
							except Exception:
								pass

					if elem.tag == 'hostname':
						hostname = elem.attrib['name']
						countHN += 1

				except Exception:
					pass

			#Agregamos cada host a la lista de servidores
			server = Host(status, ip, hostname, honeypot, ssh)
			servidores.append(server)

	return (servidores, sUP, sDOWN, countHTTP, apache, dionaea, nginx, otros, countHTTPS, countSSH, countDNS, countHN)

def escribe_reporte(datos, archivo_reporte):
	with open(archivo_reporte,'w') as output:
		output.write('Status,IP,NombreDominio,HoneyPot,SSH' + '\n')
		map(lambda u: output.write(str(u)), datos[0])

	print 'Servidores encendidos: %s'				%(datos[1])
	print 'Servidores apagados: %s'					%(datos[2])
	print 'Servidores con puerto HTTP abierto: %s'	%(datos[3])
	print 'Servidores con HTTP de Apache: %s'		%(datos[4])
	print 'Servidores con HTTP de Dionaea: %s'		%(datos[5])
	print 'Servidores con HTTP de Nginx: %s'		%(datos[6])
	print 'Servidores con otro HTTP: %s'			%(datos[7])
	print 'Servidores con HTTPS abierto: %s'		%(datos[8])
	print 'Servidores con puerto SSH abierto: %s'	%(datos[9])
	print 'Servidores con puerto DNS abierto: %s'	%(datos[10])
	print 'Servidores con nombre de dominio: %s'	%(datos[11])

if __name__ == '__main__':
    if len(sys.argv) != 3:
        printError('Indicar archivo a leer y archivo de reporte.', True)
    datos = leeXML(sys.argv[1])
    escribe_reporte(datos, sys.argv[2])
